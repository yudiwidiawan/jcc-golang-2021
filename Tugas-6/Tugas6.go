package main

import "fmt"

// soal 1
func hitungLuasLing(jariJari *float64, luas *float64) {
	*luas = 3.14 * ((*jariJari)*(*jariJari))
}
func hitungKelLing(jariJari *float64, keliling *float64) {
	*keliling = 3.14 * (2 * (*jariJari))
}
// soal 2
func introduce(sentence *string, nama, gender, pekerjaan, umur string) {
	var panggilan string
	if gender == "laki-laki" {
		panggilan = "Pak"
	} else if gender == "perempuan" {
		panggilan = "Bu"
	} else {
		panggilan = "Kak"
	}
	*sentence = panggilan + " " + nama + " adalah seorang " + pekerjaan + " yang berusia " + umur + " tahun"
}

// soal 3
func addDataBuah(buahref *[]string, buahs ...string) {
	for _, buah := range buahs {
		*buahref = append(*buahref, buah)
	}
}

// soal 4 function is a closure function located in the main function

func main() {
	// soal 1
	var luasLigkaran float64
	var kelilingLingkaran float64
	var jari float64 = 7.2
	var jariJari *float64 = &jari
	
	hitungLuasLing(jariJari, &luasLigkaran)
	hitungKelLing(jariJari, &kelilingLingkaran)
	fmt.Println("Luas lingkaran	\t:", luasLigkaran)
	fmt.Println("Keliling lingkaran	:", kelilingLingkaran)
	fmt.Println()
	
	// soal 2
	var sentence string 
	introduce(&sentence, "John", "laki-laki", "penulis", "30")
	fmt.Println(sentence)
	introduce(&sentence, "Sarah", "perempuan", "model", "28")
	fmt.Println(sentence)
	fmt.Println()
	
	// soal 3
	var buah = []string{}
	addDataBuah(&buah, "Jeruk","Semangka","Mangga","Strawberry","Durian","Manggis","Alpukat")
	for i, b := range buah {
		fmt.Printf("%d. %s\n", (i+1), b) 
	}
	fmt.Println()
	
	// soal 4
	var dataFilm = []map[string]string{}
	var tambahDataFilm = func(title, duration, genre, tahun string, data *[]map[string]string) {
		*data = append(*data, map[string]string{"title": title, "duration": duration, "genre": genre, "tahun": tahun})
	}

	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)
	
	for i, item := range dataFilm {
		j := 0
		for key, subitem := range item {
			if j == 0 {
				fmt.Printf("%d. %s : %s\n", (i+1), key, subitem)
			} else {
				fmt.Printf("   %s : %s\n", key, subitem)
			}
			j++
		}
	}

}
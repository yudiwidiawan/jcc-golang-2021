package main

import (
	"fmt"
	"sort"
	"time"
	"math"
	"flag"
	"errors"
)

// soal 1
func addItems(dest *[]string, items ...string) {
	for _, e := range items {
		*dest = append(*dest, e)
	}
}
// soal 2
func luasLingkaran(jariJari float64) float64 {
	return math.Round( math.Pi * math.Pow(jariJari, 2))
}

func kelilingLingkaran(jariJari float64) float64 {
	return math.Round(2 * math.Pi * jariJari)
}
// soal 3
func checkValuePersegiPanjang(panjang *int, lebar *int) error {
	if *panjang == 0 {
		if *lebar == 0 {
			return errors.New("Maaf, anda belum memasukkan nilai panjang dan lebar. (Tambahkan flag --panjang dan --lebar saat menjalankan program)")
		}
		return errors.New("Maaf, anda belum memasukkan nilai panjang. (Tambahkan flag --lebar saat menjalankan program)")
	} else if *lebar == 0 {
		return errors.New("Maaf, anda belum memasukkan nilai lebar. (Tambahkan flag --panjang menjalankan program)")
	}
	return nil
}
func luasPersegiPanjang(panjang *int, lebar *int) (int, error) {
	err := checkValuePersegiPanjang(panjang, lebar)
	return ((*panjang) * (*lebar)), err
}

func kelilingPersegiPanjang(panjang *int, lebar *int) (int, error) {
	err := checkValuePersegiPanjang(panjang, lebar)
	return ((2*(*panjang)) + (2*(*lebar))), err
}

func main() {
	// soal 1
	var phones = []string{}
	addItems(&phones,"Xiaomi","Asus","IPhone","Samsung","Oppo","Realme","Vivo")
	sort.Strings(phones)
	for i, e := range phones {
		fmt.Printf("%d. %s\n", (i+1), e)
		time.Sleep(time.Second * 1)
	}
	fmt.Println()
	
	// soal 2
	var jariJaris = []float64{7, 10, 15}
	for _, e := range jariJaris {
		fmt.Println("r =", e)
		fmt.Printf("Luas \t : %.2f\n", luasLingkaran(e))
		fmt.Printf("Keliling : %.2f\n", kelilingLingkaran(e))
	}
	fmt.Println()
	
	// soal 3
	var panjang = flag.Int("panjang", 0, "ketikkan panjang persegi")
	var lebar = flag.Int("lebar", 0, "ketikkan lebar persegi")
	flag.Parse()
	hasilLuas, err := luasPersegiPanjang(panjang, lebar)
	if err == nil {
		fmt.Println("Luas Persegi Panjang :", hasilLuas)
	} else {
		fmt.Println("Error :", err.Error())
	}
	hasilKeliling, err := kelilingPersegiPanjang(panjang, lebar)
	if err == nil {
		fmt.Println("Keliling Persegi Panjang :", hasilKeliling)
	} else {
		fmt.Println("Error :", err.Error())
	}
	
	
}
package main

import (
	"fmt"
)
// soal 1
func luasPersegiPanjang(p int, l int) int {
	return p*l
}
func kelilingPersegiPanjang(p int, l int) int {
	return (2*p) + (2*l)
}
func volumeBalok(p int, l int, t int) int {
	return p*l*t
}
// soal 2
func introduce(nama string, gender string, pekerjaan string, umur string) (kalimat string) {
	var panggilan string
	if gender == "laki-laki" {
		panggilan = "Pak"
	} else if gender == "perempuan" {
		panggilan = "Bu"
	} else {
		panggilan = "Kak"
	}
	kalimat = panggilan + " " + nama + " adalah seorang " + pekerjaan + " yang berusia " + umur + " tahun"
	return
}
// soal 3
func buahFavorit(nama string, buahs ...string) string{
	kalimat := `halo nama saya ` + nama + ` dan buah favorit saya adalah `
	for i, buah := range buahs {
		kalimat += `"` + buah + `"`
		if i < (len(buahs)-1) {
			kalimat += ", "
		}
	}
	return kalimat
}
// soal 4 function is a closure function located in the main function

func main() {

	// soal 1
	panjang := 12
	lebar := 4
	tinggi := 8
	  
	luas := luasPersegiPanjang(panjang, lebar)
	keliling := kelilingPersegiPanjang(panjang, lebar)
	volume := volumeBalok(panjang, lebar, tinggi)

	fmt.Println(luas) 
	fmt.Println(keliling)
	fmt.Println(volume)
	fmt.Println()

	// soal 2
	john := introduce("John", "laki-laki", "penulis", "30")
	fmt.Println(john)

	sarah := introduce("Sarah", "perempuan", "model", "28")
	fmt.Println(sarah)
	fmt.Println()
	
	// soal 3
	var buah = []string{"semangka", "jeruk", "melon", "pepaya"}
	var buahFavoritJohn = buahFavorit("John", buah...)
	
	fmt.Println(buahFavoritJohn)
	fmt.Println()
	
	// soal 4
	var dataFilm = []map[string]string{}
	var tambahDataFilm = func(title string, jam string, genre string, tahun string) {
		dataFilm = append(dataFilm, map[string]string{"genre": genre, "jam": jam, "tahun": tahun, "title": title})
	}

	tambahDataFilm("LOTR", "2 jam", "action", "1999")
	tambahDataFilm("avenger", "2 jam", "action", "2019")
	tambahDataFilm("spiderman", "2 jam", "action", "2004")
	tambahDataFilm("juon", "2 jam", "horror", "2004")

	for _, item := range dataFilm {
	   fmt.Println(item)
	}
}
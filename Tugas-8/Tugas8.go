package main

import(
	"fmt"
	"math"
	"strings"
	"strconv"
)
// soal 1
type segitigaSamaSisi struct{
  alas, tinggi int
}

type persegiPanjang struct{
  panjang, lebar int
}

type tabung struct{
  jariJari, tinggi float64
}

type balok struct{
  panjang, lebar, tinggi int
}

type hitungBangunDatar interface{
  luas() int
  keliling() int
}

type hitungBangunRuang interface{
  volume() float64
  luasPermukaan() float64
}

func (ssi segitigaSamaSisi) luas() int{
	return (ssi.alas * ssi.tinggi) / 2
}

func (ssi segitigaSamaSisi) keliling() int{
	return ssi.alas+ssi.alas+ssi.alas
}

func (pp persegiPanjang) luas() int{
	return pp.panjang * pp.lebar
}

func (pp persegiPanjang) keliling() int{
	return (2*pp.panjang) + (2*pp.lebar)
}

func (tb tabung) volume() float64{
	return math.Pi * math.Pow(tb.jariJari, 2) * tb.tinggi
}

func (tb tabung) luasPermukaan() float64{
	return (2 * math.Pi * math.Pow(tb.jariJari, 2)) + (2 * math.Pi * tb.jariJari * tb.tinggi)
}

func (bk balok) volume() float64{
	return float64(bk.panjang * bk.lebar * bk.tinggi)
}

func (bk balok) luasPermukaan() float64{
	return float64((2 * bk.panjang * bk.lebar) + (2 * bk.panjang * bk.tinggi) + (2 * bk.lebar * bk.tinggi))
}

// soal 2
type phone struct{
   name, brand string
   year int
   colors []string
}

type phoneInfo interface{
	viewPhoneInfo() string
}

func (ph phone) viewPhoneInfo() string {
	var colors = strings.Join(ph.colors, ", ") 
	return "name : " + ph.name + "\nbrand : " + ph.brand + "\nyear : " + strconv.Itoa(ph.year) + "\ncolors : " + colors
}

// soal 3
func luasPersegi(sisi int, tekstual bool) interface{}{
	if tekstual {
		if sisi == 0 {
			return "Maaf anda belum menginput sisi dari persegi"
		}
		return "luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi * sisi) + " cm"
	} else {
		if sisi == 0 {
			return nil
		}
		return sisi * sisi
	}
}

// soal 4 is in the main function

func main() {
	// soal 1
	var bangunDatar hitungBangunDatar
	bangunDatar = segitigaSamaSisi{6.0, 8.0}
	fmt.Println("\nSoal 1")
	fmt.Println("=====Segitiga Sama Sisi")
	fmt.Println("Alas = ", bangunDatar.(segitigaSamaSisi).alas, ", Tinggi = ", bangunDatar.(segitigaSamaSisi).tinggi)
	fmt.Println("Luas = ", bangunDatar.luas(), ", Keliling = ", bangunDatar.keliling())
	bangunDatar = persegiPanjang{3.0, 7.0}
	fmt.Println("=====Persegi Panjang")
	fmt.Println("Panjang = ", bangunDatar.(persegiPanjang).panjang, ", Lebar = ", bangunDatar.(persegiPanjang).lebar)
	fmt.Println("Luas = ", bangunDatar.luas(), ", Keliling = ", bangunDatar.keliling())
	
	var bangunRuang hitungBangunRuang
	bangunRuang = tabung{7.0, 28.0}
	fmt.Println("=====Tabung")
	fmt.Println("Jari-jari = ", bangunRuang.(tabung).jariJari, ", Tinggi = ", bangunRuang.(tabung).tinggi)
	fmt.Println("Volume = ", bangunRuang.volume(), ", Luas Permukaan = ", bangunRuang.luasPermukaan())
	bangunRuang = balok{10.0, 5.0, 5.0}
	fmt.Println("=====Balok")
	fmt.Println("Panjang = ", bangunRuang.(balok).panjang, ", Lebar = ", bangunRuang.(balok).lebar,
	", Tinggi = ", bangunRuang.(balok).tinggi)
	fmt.Println("Volume = ", bangunRuang.volume(), ", Luas Permukaan = ", bangunRuang.luasPermukaan(), "\n")
	
	// soal 2
	fmt.Println("\nSoal 2")
	var samsung phoneInfo
	samsung = phone{"Samsung Galaxy Note 20", "Samsung Galaxy Note 20", 2020, []string{"Mystic Bronze",
	"Mystic White", "Mystic Black"}}
	fmt.Println(samsung.viewPhoneInfo())
	
	// soal 3
	fmt.Println("\nSoal 3")
	fmt.Println(luasPersegi(4, true))
	fmt.Println(luasPersegi(8, false))
	fmt.Println(luasPersegi(0, true))
	fmt.Println(luasPersegi(0, false))
	
	// soal 4
	fmt.Println("\nSoal 4")
	var prefix interface{}= "hasil penjumlahan dari "
	var kumpulanAngkaPertama interface{} = []int{6,8}
	var kumpulanAngkaKedua interface{} = []int{12,14}
	var hasil int
	for _, e := range kumpulanAngkaPertama.([]int) {
		hasil += e
	}
	for _, e := range kumpulanAngkaKedua.([]int) {
		hasil += e
	}
	var gabungan = prefix.(string) + strconv.Itoa(kumpulanAngkaPertama.([]int)[0]) +
	" + " + strconv.Itoa(kumpulanAngkaPertama.([]int)[1]) + " + " +
	strconv.Itoa(kumpulanAngkaKedua.([]int)[0]) + " + " +
	strconv.Itoa(kumpulanAngkaKedua.([]int)[1]) + " = " + strconv.Itoa(hasil)
	fmt.Println(gabungan)
	
}
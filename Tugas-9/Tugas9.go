package main

import (
	"fmt"
	"errors"
	"strconv"
)

// soal 1
func footPrint(kalimat string, tahun int) {
	fmt.Println(kalimat, tahun)
}

// soal 2
func kelilingSegitigaSamaSisi(sisi int, tekstual bool) (string, error) {
	if tekstual {
		if sisi == 0 {
			return string(sisi), errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
		}
		return "keliling segitiga sama sisinya dengan sisi " + strconv.Itoa(sisi) + " cm adalah " +
		strconv.Itoa(sisi + sisi + sisi) + " cm", nil
	} else {
		if sisi == 0 {
			panic("Maaf anda belum menginput sisi dari segitiga sama sisi")
			return strconv.Itoa(sisi), errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
		}
		return strconv.Itoa(sisi + sisi + sisi), nil
	}
}
func cetakHasilHitungKeliling(h string, e error) {
	if e == nil {
		fmt.Println(h)
	} else {
		fmt.Println(e)
	}
}

// soal 3
func tambahAngka(angka int, temp *int) {
	*temp = *temp + angka
}

func main() {
	// soal 3
	angka := 1
	defer func() {
		tambahAngka(7, &angka)
		tambahAngka(6, &angka)
		tambahAngka(-1, &angka)
		tambahAngka(9, &angka)
		fmt.Println("Total :", angka)
	}()

	// soal 1
	defer footPrint("Candradimuka Jabar Coding Camp", 2021)
	
	// soal 2
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered :", r, "\n")
		}
	}()
	hasil, err := kelilingSegitigaSamaSisi(4, true)
	cetakHasilHitungKeliling(hasil, err)
	hasil, err = kelilingSegitigaSamaSisi(8, false)
	cetakHasilHitungKeliling(hasil, err)
	hasil, err = kelilingSegitigaSamaSisi(0, true)
	cetakHasilHitungKeliling(hasil, err)
	hasil, err = kelilingSegitigaSamaSisi(0, false)
	cetakHasilHitungKeliling(hasil, err)
	
}
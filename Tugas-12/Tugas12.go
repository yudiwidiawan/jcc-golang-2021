package main

import (
	"fmt"
	"sync"
	"sort"
	"time"
	"strconv"
	"math"
)

// soal 1
func cetakBrand(idx int, brand string, wg *sync.WaitGroup) {
	fmt.Printf("%d. %s\n", idx, brand)
	wg.Done()
}
// soal 2
func getMovies(mvch chan string, movies ...string) {
	for i, e := range movies {
		mvch <- strconv.Itoa(i+1) + ". " + e
	}
	close(mvch)
}
// soal 3
func luasLingkaran(jariJari int, ch chan float64) {
	ch <- float64(math.Pi * math.Pow(float64(jariJari), 2))
}
func kelilingLingkaran(jariJari int, ch chan float64) {
	ch <- float64(math.Pi * float64(2*jariJari))
}
func volumeTabung(jariJari int, tinggi int, ch chan float64) {
	ch <- float64(math.Pi * math.Pow(float64(jariJari), 2) * float64(tinggi))
}
// soal 4
func luasPersegiPanjang(pj int, lb int, ch chan int) {
	ch <- pj * lb
}
func kelilingPersegiPanjang(pj int, lb int, ch chan int) {
	ch <- (2 * pj) + (2 * lb)
}
func volumeBalok(pj int, lb int, tg int, ch chan int) {
	ch <- pj * lb * tg
}

func main () {
	// soal 1
	var wg sync.WaitGroup
	var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}
	sort.Strings(phones)
	for i, e := range phones {
		wg.Add(1)
		go cetakBrand((i+1), e, &wg)
		time.Sleep(time.Second * 1)
	}
	wg.Wait(); fmt.Println();
	
	// soal 2
	var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}
	moviesChannel := make(chan string)
	go getMovies(moviesChannel, movies...)
	for value := range moviesChannel {
	  fmt.Println(value)
	}
	fmt.Println()
	
	// soal 3
	jariJaries, tinggi := []int{8, 14, 20}, 10
	for _, e := range jariJaries {
		lk1, kl1, vl1 := make(chan float64), make(chan float64), make(chan float64)
		go luasLingkaran(e, lk1); go kelilingLingkaran(e, kl1); go volumeTabung(e, tinggi, vl1);
		hasilLuas, hasilKeliling, hasilVolume := <- lk1, <- kl1, <- vl1
		fmt.Printf("Jari-jari = %d, Tinggi = %d\n", e, tinggi)
		fmt.Printf("Luas Lingkaran : %.2f\nKeliling Lingkaran : %.2f\nVolume Tabung : %.2f\n",
		hasilLuas, hasilKeliling, hasilVolume)
		fmt.Printf("-----------------------------\n")
	}
	fmt.Println()
	// soal 4
	chLuas, chKel, chVol := make(chan int), make(chan int), make(chan int)
	go luasPersegiPanjang(10, 5, chLuas); go kelilingPersegiPanjang(10, 5, chKel);
	go volumeBalok(10, 5, 8, chVol)
	for i := 0; i < 3; i++ {
		select {
			case luas := <- chLuas:
				fmt.Printf("Luas Persegi Panjang : %d\n", luas)
			case keliling := <- chKel:
				fmt.Printf("Keliling Persegi Panjang : %d\n", keliling)
			case volume := <- chVol:
				fmt.Printf("Volume Balok : %d\n", volume)
		}
	}
}
package main

import ( 
	"fmt"
	"strconv"
	)

func main () {
	// soal 1	
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"

	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"
	
	panjangPPInt, err := strconv.Atoi(panjangPersegiPanjang)
	lebarPPInt, err := strconv.Atoi(lebarPersegiPanjang)
	
	alasSegitigaInt, err := strconv.Atoi(alasSegitiga)
	tinggiSegitigaInt, err := strconv.Atoi(tinggiSegitiga)
	
	if err == nil {
		var kelilingPersegiPanjang int = (2 * panjangPPInt) + (2 * lebarPPInt)
		var luasSegitiga int = (alasSegitigaInt * tinggiSegitigaInt)/2
		fmt.Printf("Persegi Panjang\n P= %d, L= %d\n", panjangPPInt, lebarPPInt)
		fmt.Printf("Keliling = %d\n", kelilingPersegiPanjang)
		fmt.Printf("\nSegitiga\n a= %d, t= %d\n", alasSegitigaInt, tinggiSegitigaInt)
		fmt.Printf("Luas = %d\n\n", luasSegitiga)
	}
	
	// soal 2
	var nilaiJohn = 80
	var nilaiDoe = 50
	var indexJohn, indexDoe rune
	
	if(nilaiJohn >= 80) {
		indexJohn = 'A'
	} else if (nilaiJohn >= 70) && (nilaiJohn < 80) {
		indexJohn = 'B'
	} else if (nilaiJohn >= 60) && (nilaiJohn < 70) {
		indexJohn = 'C'
	} else if (nilaiJohn >= 50) && (nilaiJohn < 60) {
		indexJohn = 'D'
	} else if (nilaiJohn < 50) {
		indexJohn = 'E'
	}
	
	if(nilaiDoe >= 80) {
		indexDoe = 'A'
	} else if (nilaiDoe >= 70) && (nilaiDoe < 80) {
		indexDoe = 'B'
	} else if (nilaiDoe >= 60) && (nilaiDoe < 70) {
		indexDoe = 'C'
	} else if (nilaiDoe >= 50) && (nilaiDoe < 60) {
		indexDoe = 'D'
	} else if (nilaiDoe < 50) {
		indexDoe = 'E'
	}
	
	fmt.Printf("Nama  Nilai  Indeks\n")
	fmt.Printf("John  %d 	%s\n", nilaiJohn, string(indexJohn)) 
	fmt.Printf("Doe   %d 	%s\n\n", nilaiDoe, string(indexDoe))
	
	// soal 3
	var tanggal = 24;
	var bulan = 5; 
	var tahun = 1996;
	
	var bulanString string
	
	switch bulan {
		case 1:
			bulanString = "Januari"
		case 2:
			bulanString = "Februari"
		case 3:
			bulanString = "Maret"
		case 4:
			bulanString = "April"
		case 5:
			bulanString = "Mei"
		case 6:
			bulanString = "Juni"
		case 7:
			bulanString = "Juli"
		case 8:
			bulanString = "Agustus"
		case 9:
			bulanString = "September"
		case 10:
			bulanString = "Oktober"
		case 11:
			bulanString = "November"
		case 12:
			bulanString = "Desember"
		default:
			bulanString = "Bulan tidak ditemukan."
	}
	
	fmt.Printf("%d %s %d\n\n", tanggal, bulanString, tahun)
	
	// soal 4
	var generasi string
	
	if tahun >= 1944 && tahun <= 1964 {
		generasi = "Baby boomer"
	} else if tahun >= 1965 && tahun <= 1979 {
		generasi = "X"
	} else if tahun >= 1980 && tahun <= 1994 {
		generasi = "Y (Millenials)"
	} else if tahun >= 1995 && tahun <= 2015 {
		generasi = "Z"
	}
	
	fmt.Printf("Anda lahir tahun %d, anda adalah generasi %s.\n\n", tahun, generasi)
	
	
}
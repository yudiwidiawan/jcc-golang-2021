package main

import "fmt"

func main () {

	// soal 1
	var kata = [3]string{"JCC", "Candradimuka", "I Love Coding"}
	for i:=1; i <=20; i++ {
		if i % 2 == 1 {
			if i % 3 == 0 {
				fmt.Printf("%d - %s\n", i, kata[2])
			} else {
				fmt.Printf("%d - %s\n", i, kata[0])
			}
		} else if i % 2 == 0 {
			fmt.Printf("%d - %s\n", i, kata[1])
		}
	}
	
	// soal 2
	for i:=0; i <8; i++ {
		for j:=0; j<i; j++ {
			fmt.Print("#")
		}
		fmt.Println()
	}
	fmt.Println()
	
	// soal 3
	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}
	fmt.Println(kalimat[2:]);
	
	// soal 4
	var sayuran = []string{}
	sayuran = append(sayuran, "Bayam", "Buncis", "Kangkung", "Kubis", "Seledri", "Tauge", "Timun");
	fmt.Println()
	for i, sayur := range sayuran {
		fmt.Printf("%d. %s\n", (i+1), sayur) 
	}
	fmt.Println()
	
	// soal 5
	var satuan = map[string]int{	  
	  "lebar":   4,
	  "panjang": 7,
	  "tinggi":  6,
	}
	var volume int = 1
	var copySatuan = satuan
	var keysat = []string{"panjang","lebar","tinggi"}
	for {
		for i:=0; i < len(keysat); i++ {
			if copySatuan[keysat[i]] != 0 {
				fmt.Printf("%s = %d\n", keysat[i], satuan[keysat[i]])
				volume *= satuan[keysat[i]]
				delete(copySatuan, keysat[i]);
				break
			}
		}
		if len(copySatuan) == 0 {
			break
		}
	}
	fmt.Println("Volume balok = ", volume)
}
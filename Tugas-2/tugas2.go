package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// soal 1
	var kata1 = "Jabar"
	var kata2 = "Coding"
	var kata3 = "Camp"
	var kata4 = "Golang"
	var kata5 = "2021"

	fmt.Println(kata1 + " " + kata2 + " " + kata3 + " " + kata4 + " " + kata5)

	// soal 2
	halo := "Halo Dunia"
	halo = strings.Replace(halo, "Dunia", "Golang", 5)
	fmt.Println(halo)

	// soal 3
	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"

	fmt.Println(
		kataPertama + " " +
			strings.Title(kataKedua) + " " +
			strings.Replace(kataKetiga, "r", "R", 6) + " " +
			strings.ToUpper(kataKeempat))

	// soal 4
	var angkaPertama = "8"
	var angkaKedua = "5"
	var angkaKetiga = "6"
	var angkaKeempat = "7"

	var iAngkaPertama, err = strconv.Atoi(angkaPertama)
	var iAngkaKedua, err2 = strconv.Atoi(angkaKedua)
	var iAngkaKetiga, err3 = strconv.Atoi(angkaKetiga)
	var iAngkaKeempat, err4 = strconv.Atoi(angkaKeempat)
	if err == nil && err2 == nil && err3 == nil && err4 == nil {
		jumlah := iAngkaPertama + iAngkaKedua + iAngkaKetiga + iAngkaKeempat
		fmt.Printf("%d+%d+%d+%d = %d\n", iAngkaPertama, iAngkaKedua, iAngkaKetiga, iAngkaKeempat, jumlah)
	}

	// soal 5
	kalimat := `"halo halo bandung"`
	angka := 2021

	fmt.Printf(strings.ReplaceAll(kalimat, "halo", "Hi")+" - %d", angka)

}

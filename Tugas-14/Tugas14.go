package main

import (
	"encoding/json"
	"net/http"	
	"strconv"
	"errors"
	"log"
	"fmt"
)

type NilaiMahasiswa struct{
  Nama, MataKuliah, IndeksNilai string
  Nilai, ID uint
}

var nilaiNilaiMahasiswa = []NilaiMahasiswa{}

func checkNilai(nilai int, errorconv error) (int, error) {
	if errorconv != nil {
		return nilai, errorconv
	}
	if nilai > 100 {
		return nilai, errors.New("Maaf, nilai tidak boleh lebih dari 100.")
	} else if nilai <= -1 {
		return nilai, errors.New("Maaf, nilai tidak boleh kurang dari 0.")
	}
	return nilai, nil
}

func GetIndeksNilai(nilai int, w http.ResponseWriter, r *http.Request) string {
	switch {
		case nilai >= 80:
			return "A"
		case nilai >= 70 && nilai < 80:
			return "B"
		case nilai >= 60 && nilai < 70:
			return "C"
		case nilai >= 50 && nilai < 60:
			return "D"
		case nilai < 50:
			return "E"
	}
	return "F"
}

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		username, password, ok := r.BasicAuth()
		if !ok {
			w.Write([]byte("Username atau password tidak boleh kosong!\n"))
			return
		}
		if username == "admin" && password == "admin" {
			next.ServeHTTP(w, r)
			return
		}
		w.Write([]byte("Username atau password salah!\n"))
		return
	})
}

func SaveMahasiswa(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var Nilai NilaiMahasiswa
	if r.Method == "POST" {
		if r.Header.Get("Content-Type") == "application/json" {
			decodeJSON := json.NewDecoder(r.Body)
			if err := decodeJSON.Decode(&Nilai); err != nil {
				if _, ok := err.(*json.UnmarshalTypeError); ok {
					http.Error(w, "Gagal konversi data JSON, periksa kembali data masukan.", http.StatusInternalServerError)
					return
				} else {
					log.Fatal(err)
				}
			}
			Nilai.ID = uint(len(nilaiNilaiMahasiswa) + 1)
			converted, errnilai := checkNilai(int(Nilai.Nilai), nil)
			if errnilai != nil {
				http.Error(w, "Error : " + errnilai.Error(), http.StatusInternalServerError)
				return
			}
			Nilai.IndeksNilai = GetIndeksNilai(converted, w, r)
		} else {
			id := uint(len(nilaiNilaiMahasiswa) + 1)
			nama := r.PostFormValue("nama")
			matakuliah := r.PostFormValue("matakuliah")
			converted, errnilai := checkNilai(strconv.Atoi(r.PostFormValue("nilai")))
			if errnilai != nil {
				http.Error(w, "Error : " + errnilai.Error(), http.StatusInternalServerError)
				return
			}
			nilai := uint(converted)
			indeksnilai := GetIndeksNilai(converted, w, r)
			
			Nilai = NilaiMahasiswa {
				ID : id,
				Nama : nama,
				MataKuliah : matakuliah,
				Nilai : nilai,
				IndeksNilai : indeksnilai,
			}
		}
		nilaiNilaiMahasiswa = append(nilaiNilaiMahasiswa, Nilai)
		dataNilai, _ := json.Marshal(Nilai)
		w.Write(dataNilai)
		return
	}
	http.Error(w, "Error : 404 Not Found", http.StatusNotFound)
	return
}

func ViewMahasiswa(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		dataMahasiswa, err := json.Marshal(nilaiNilaiMahasiswa)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(dataMahasiswa)
		return
	}
	
	http.Error(w, "Error : 404 Not Found", http.StatusNotFound)
	return
}

func main() {
	server := &http.Server {
		Addr: "127.0.0.1:8080",
	}
	
	http.Handle("/", Auth(http.HandlerFunc(SaveMahasiswa)))
	http.Handle("/mahasiswa", http.HandlerFunc(ViewMahasiswa))
	fmt.Println("Server berjalan pada http://localhost:8080")
	server.ListenAndServe()
}
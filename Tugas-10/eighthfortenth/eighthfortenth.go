package eighthfortenth

import(
	"math"
	"strings"
	"strconv"
)
// soal 1
type SegitigaSamaSisi struct{
  Alas, Tinggi int
}

type PersegiPanjang struct{
  Panjang, Lebar int
}

type Tabung struct{
  JariJari, Tinggi float64
}

type Balok struct{
  Panjang, Lebar, Tinggi int
}

type HitungBangunDatar interface{
  Luas() int
  Keliling() int
}

type HitungBangunRuang interface{
  Volume() float64
  LuasPermukaan() float64
}

func (ssi SegitigaSamaSisi) Luas() int{
	return (ssi.Alas * ssi.Tinggi) / 2
}

func (ssi SegitigaSamaSisi) Keliling() int{
	return ssi.Alas+ssi.Alas+ssi.Alas
}

func (pp PersegiPanjang) Luas() int{
	return pp.Panjang * pp.Lebar
}

func (pp PersegiPanjang) Keliling() int{
	return (2*pp.Panjang) + (2*pp.Lebar)
}

func (tb Tabung) Volume() float64{
	return math.Pi * math.Pow(tb.JariJari, 2) * tb.Tinggi
}

func (tb Tabung) LuasPermukaan() float64{
	return (2 * math.Pi * math.Pow(tb.JariJari, 2)) + (2 * math.Pi * tb.JariJari * tb.Tinggi)
}

func (bk Balok) Volume() float64{
	return float64(bk.Panjang * bk.Lebar * bk.Tinggi)
}

func (bk Balok) LuasPermukaan() float64{
	return float64((2 * bk.Panjang * bk.Lebar) + (2 * bk.Panjang * bk.Tinggi) + (2 * bk.Lebar * bk.Tinggi))
}

// soal 2
type Phone struct{
   Name, Brand string
   Year int
   Colors []string
}

type PhoneInfo interface{
	ViewPhoneInfo() string
}

func (ph Phone) ViewPhoneInfo() string {
	var colors = strings.Join(ph.Colors, ", ") 
	return "name : " + ph.Name + "\nbrand : " + ph.Brand + "\nyear : " + strconv.Itoa(ph.Year) + "\ncolors : " + colors
}

// soal 3
func LuasPersegi(sisi int, tekstual bool) interface{}{
	if tekstual {
		if sisi == 0 {
			return "Maaf anda belum menginput sisi dari persegi"
		}
		return "luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi * sisi) + " cm"
	} else {
		if sisi == 0 {
			return nil
		}
		return sisi * sisi
	}
}

// soal 4 is in the main function in the main.go
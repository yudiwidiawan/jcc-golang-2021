package main

import (
	. "Tugas-10/eighthfortenth"
	"fmt"
	"strconv"
)

func main() {
	// soal 1
	var bangunDatar HitungBangunDatar
	bangunDatar = SegitigaSamaSisi{6.0, 8.0}
	fmt.Println("\nSoal 1")
	fmt.Println("=====Segitiga Sama Sisi")
	fmt.Println("Alas = ", bangunDatar.(SegitigaSamaSisi).Alas, ", Tinggi = ", bangunDatar.(SegitigaSamaSisi).Tinggi)
	fmt.Println("Luas = ", bangunDatar.Luas(), ", Keliling = ", bangunDatar.Keliling())
	bangunDatar = PersegiPanjang{3.0, 7.0}
	fmt.Println("=====Persegi Panjang")
	fmt.Println("Panjang = ", bangunDatar.(PersegiPanjang).Panjang, ", Lebar = ", bangunDatar.(PersegiPanjang).Lebar)
	fmt.Println("Luas = ", bangunDatar.Luas(), ", Keliling = ", bangunDatar.Keliling())
	
	var bangunRuang HitungBangunRuang
	bangunRuang = Tabung{7.0, 28.0}
	fmt.Println("=====Tabung")
	fmt.Println("Jari-jari = ", bangunRuang.(Tabung).JariJari, ", Tinggi = ", bangunRuang.(Tabung).Tinggi)
	fmt.Println("Volume = ", bangunRuang.Volume(), ", Luas Permukaan = ", bangunRuang.LuasPermukaan())
	bangunRuang = Balok{10.0, 5.0, 5.0}
	fmt.Println("=====Balok")
	fmt.Println("Panjang = ", bangunRuang.(Balok).Panjang, ", Lebar = ", bangunRuang.(Balok).Lebar,
	", Tinggi = ", bangunRuang.(Balok).Tinggi)
	fmt.Println("Volume = ", bangunRuang.Volume(), ", Luas Permukaan = ", bangunRuang.LuasPermukaan(), "\n")
	
	// soal 2
	fmt.Println("\nSoal 2")
	var samsung PhoneInfo
	samsung = Phone{"Samsung Galaxy Note 20", "Samsung Galaxy Note 20", 2020, []string{"Mystic Bronze",
	"Mystic White", "Mystic Black"}}
	fmt.Println(samsung.ViewPhoneInfo())
	
	// soal 3
	fmt.Println("\nSoal 3")
	fmt.Println(LuasPersegi(4, true))
	fmt.Println(LuasPersegi(8, false))
	fmt.Println(LuasPersegi(0, true))
	fmt.Println(LuasPersegi(0, false))
	
	// soal 4
	fmt.Println("\nSoal 4")
	var prefix interface{}= "hasil penjumlahan dari "
	var kumpulanAngkaPertama interface{} = []int{6,8}
	var kumpulanAngkaKedua interface{} = []int{12,14}
	var hasil int
	for _, e := range kumpulanAngkaPertama.([]int) {
		hasil += e
	}
	for _, e := range kumpulanAngkaKedua.([]int) {
		hasil += e
	}
	var gabungan = prefix.(string) + strconv.Itoa(kumpulanAngkaPertama.([]int)[0]) +
	" + " + strconv.Itoa(kumpulanAngkaPertama.([]int)[1]) + " + " +
	strconv.Itoa(kumpulanAngkaKedua.([]int)[0]) + " + " +
	strconv.Itoa(kumpulanAngkaKedua.([]int)[1]) + " = " + strconv.Itoa(hasil)
	fmt.Println(gabungan)
}
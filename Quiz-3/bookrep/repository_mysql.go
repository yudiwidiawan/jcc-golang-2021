package bookrep

import (
	"Quiz-3/config"
	"Quiz-3/models"
	"context"
	"fmt"
	"log"
	"time"
	"database/sql"
	"errors"
	"strconv"
	"strings"
	"net/http"
)

const (
	table = "book"
	layoutDateTime = "2006-01-02 15:04:05"
)

func GetAll(ctx context.Context) ([]models.Book, error) {
	var books []models.Book
	db, err := config.MySQL()
	
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at ASC", table)
	rowQuery, err := db.QueryContext(ctx, queryText)
	
	if err != nil {
		log.Fatal(err)
	}
	
	for rowQuery.Next() {
		var book models.Book
		var createdAt, updatedAt, price string
		if err = rowQuery.Scan(&book.ID,
		&book.Title,
		&book.Description,
		&book.ImageURL,
		&book.ReleaseYear,
		&price,
		&book.TotalPage,
		&book.KategoriKetebalan,
		&createdAt,
		&updatedAt); err != nil {
			return nil, err
		}
		
		temp := strings.ReplaceAll(price, "Rp", "")
		temp = strings.ReplaceAll(temp, ".", "")
		temp = strings.ReplaceAll(temp, ",", "")
		temp = strings.ReplaceAll(temp, "-", "")
		temp = strings.ReplaceAll(temp, " ", "")
		
		book.Price, err = strconv.Atoi(temp)
		
		if err != nil {
			log.Fatal(err)
		}
		
		book.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		
		if err != nil {
			log.Fatal(err)
		}
		
		book.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		
		if err != nil {
			log.Fatal(err)
		}
		
		books = append(books, book)
	}
	return books, nil
}

func GetAllFiltered(ctx context.Context, r *http.Request) ([]models.Book, error) {
	var books []models.Book
	db, err := config.MySQL()
	
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	
	var filterQ string = ""
	var sortBy string = " "
	if len(r.URL.Query().Get("title")) > 0 {
		filterQ = fmt.Sprintf("%s AND title LIKE '%%%s%%' ", filterQ, r.URL.Query().Get("title"))
	}	
	if len(r.URL.Query().Get("sort")) > 0 {
		sortBy = fmt.Sprintf("%s", r.URL.Query().Get("sort"))
	}
	if len(r.URL.Query().Get("minYear")) > 0 {
		minYear, _ := strconv.Atoi(r.URL.Query().Get("minYear"))
		if len(r.URL.Query().Get("maxYear")) > 0 {
			maxYear, _ := strconv.Atoi(r.URL.Query().Get("maxYear"))
			filterQ = fmt.Sprintf("%s AND release_year BETWEEN %d AND %d ", filterQ, minYear, maxYear)
		} else {
			filterQ = fmt.Sprintf("%s AND release_year >= %d ", filterQ, minYear)
		}
	} else if len(r.URL.Query().Get("maxYear")) > 0 {
		maxYear, _ := strconv.Atoi(r.URL.Query().Get("maxYear"))
		filterQ = fmt.Sprintf("%s AND release_year <= %d ",filterQ, maxYear)
	}
	
	if len(r.URL.Query().Get("minPage")) > 0 {
		minPage, _ := strconv.Atoi(r.URL.Query().Get("minPage"))
		if len(r.URL.Query().Get("maxPage")) > 0 {
			maxPage, _ := strconv.Atoi(r.URL.Query().Get("maxPage"))
			filterQ = fmt.Sprintf("%s AND CAST(total_page as SIGNED INTEGER) BETWEEN %d AND %d ", filterQ, minPage, maxPage)
		} else {
			filterQ = fmt.Sprintf("%s AND CAST(total_page as SIGNED INTEGER) >= %d ", filterQ, minPage)
		}
	} else if len(r.URL.Query().Get("maxPage")) > 0 {
		maxPage, _ := strconv.Atoi(r.URL.Query().Get("maxPage"))
		filterQ = fmt.Sprintf("%s AND CAST(total_page as SIGNED INTEGER) <= %d ",filterQ, maxPage)
	}
	
	
	queryText := fmt.Sprintf("SELECT * FROM %v WHERE 1=1 %s  Order By created_at %s", table, filterQ, sortBy)
	rowQuery, err := db.QueryContext(ctx, queryText)
	
	if err != nil {
		log.Fatal(err)
	}
	
	for rowQuery.Next() {
		var book models.Book
		var createdAt, updatedAt, price string
		if err = rowQuery.Scan(&book.ID,
		&book.Title,
		&book.Description,
		&book.ImageURL,
		&book.ReleaseYear,
		&price,
		&book.TotalPage,
		&book.KategoriKetebalan,
		&createdAt,
		&updatedAt); err != nil {
			return nil, err
		}
		
		temp := strings.ReplaceAll(price, "Rp", "")
		temp = strings.ReplaceAll(temp, ".", "")
		temp = strings.ReplaceAll(temp, ",", "")
		temp = strings.ReplaceAll(temp, "-", "")
		temp = strings.ReplaceAll(temp, " ", "")
		
		book.Price, err = strconv.Atoi(temp)
		
		if err != nil {
			log.Fatal(err)
		}
		
		book.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		
		if err != nil {
			log.Fatal(err)
		}
		
		book.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		
		if err != nil {
			log.Fatal(err)
		}
		
		books = append(books, book)
	}
	return books, nil
}

func substr(input string, start int, length int) string {
    asRunes := []rune(input)
    
    if start >= len(asRunes) {
        return ""
    }
    
    if start+length > len(asRunes) {
        length = len(asRunes) - start
    }
    
    return string(asRunes[start : start+length])
}

func convertPrice(price int) string {
  temp := strconv.Itoa(price)
  var converted string
  switch len(temp) {
	case 4:
		converted = fmt.Sprintf("Rp. %s.%s ,-", substr(temp, 0, 1), substr(temp, 1, len(temp)))
	case 5:
		converted = fmt.Sprintf("Rp. %s.%s ,-", substr(temp, 0, 2), substr(temp, 2, len(temp)))
	case 6:
		converted = fmt.Sprintf("Rp. %s.%s ,-", substr(temp, 0, 3), substr(temp, 3, len(temp)))
	case 7:
		converted = fmt.Sprintf("Rp. %s.%s.%s ,-", substr(temp, 0, 1), substr(temp, 1, 3),substr(temp, 4, len(temp)))
	case 8:
		converted = fmt.Sprintf("Rp. %s.%s.%s ,-", substr(temp, 0, 2), substr(temp, 2, 3),substr(temp, 5, len(temp)))
  }
  return converted
}

// Insert Book
func Insert(ctx context.Context, book models.Book) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }
  converted := convertPrice(book.Price)
 
  queryText := fmt.Sprintf("INSERT INTO %v (title, description, image_url, release_year, price, total_page, kategori_ketebalan, created_at, updated_at) values('%v','%v','%v', %v, '%v', '%v', '%v', NOW(), NOW())", table,
    book.Title,
	book.Description,
	book.ImageURL,
	book.ReleaseYear,
	converted,
	book.TotalPage,
	book.KategoriKetebalan)
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update Book
func Update(ctx context.Context, book models.Book, id string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  converted := convertPrice(book.Price)
  queryText := fmt.Sprintf("UPDATE %v set title ='%s', description ='%s', image_url ='%s',release_year = %d, price ='%s', total_page ='%s', kategori_ketebalan ='%s', updated_at = NOW() where id = %s",
    table,
    book.Title,
	book.Description,
	book.ImageURL,
	book.ReleaseYear,
	converted,
	book.TotalPage,
	book.KategoriKetebalan,
    id,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Book
func Delete(ctx context.Context, id string) error {
    db, err := config.MySQL()
    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}
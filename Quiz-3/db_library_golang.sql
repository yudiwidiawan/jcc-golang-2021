-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Sep 2021 pada 16.33
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_library_golang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image_url` text NOT NULL,
  `release_year` int(11) NOT NULL,
  `price` varchar(100) NOT NULL,
  `total_page` varchar(255) NOT NULL,
  `kategori_ketebalan` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `book`
--

INSERT INTO `book` (`id`, `title`, `description`, `image_url`, `release_year`, `price`, `total_page`, `kategori_ketebalan`, `created_at`, `updated_at`) VALUES
(1, 'Book 1', 'Ini adalah buku', 'http://contoh.com/abc.jpg', 2000, 'Rp. 8.000 ,-', '100', 'Tipis', '2021-09-18 19:31:03', '2021-09-18 19:31:03'),
(2, 'Book Abrakadabrah', 'Ini adalah buku 2', 'http://contoh.com/abc.jpg', 2019, 'Rp. 1.234.567 ,-', '200', 'Sedang', '2021-09-18 19:48:42', '2021-09-18 21:20:23'),
(3, 'Book 3', 'Ini adalah buku', 'http://contoh.com/abc.jpg', 2010, 'Rp. 80.000.000 ,-', '203', 'Sedang', '2021-09-18 20:03:52', '2021-09-18 20:03:52'),
(4, 'Book 4', 'Ini adalah buku', 'http://contoh.com/abc.jpg', 2021, 'Rp. 12.345.678 ,-', '200', 'Sedang', '2021-09-18 20:04:15', '2021-09-18 20:04:15');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

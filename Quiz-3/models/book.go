package models

import (
	"time"
)

type (

	Book struct {
		ID int `json:"id"`
		Title string `json:"title"`
		Description string `json:"description"`
		ImageURL string `json:"image_url"`
		ReleaseYear int `json:"release_year"`
		Price int `json:"price"` // dari soal disuruh int waktu input raw json, dan waktu input di db diformat pakai Rp. x.x,-
		TotalPage string `json:"total_page"`
		KategoriKetebalan string `json:"kategori_ketebalan"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
	}

)
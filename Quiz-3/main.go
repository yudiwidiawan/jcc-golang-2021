package main

import (
	"Quiz-3/utils"
	"Quiz-3/bookrep"
	"Quiz-3/models"
	"net/http"
	"log"
	"fmt"
	"context"
	"strconv"
	"encoding/json"
	"net/url"
	
	"github.com/julienschmidt/httprouter"
)

func main() {
	router := httprouter.New()
	// Soal 2
	router.GET("/bangun-datar/:jenis", HitungBangunDatar)
	
	// Soal 3 - 5
	router.GET("/books", BasicAuth(GetBook))
	router.POST("/books/create", BasicAuth(PostBook))
	router.PUT("/books/:id/update", BasicAuth(UpdateBook))
	router.DELETE("/books/:id/delete", BasicAuth(DeleteBook))
	fmt.Println("Server running on http://localhost:8080/")
	log.Fatal(http.ListenAndServe("127.0.0.1:8080", router))
}
// Soal 2
func HitungBangunDatar(w http.ResponseWriter, r *http.Request, hr httprouter.Params) {
	if r.Method == "GET" {
		jenis, dataBangun, err := utils.CheckJenisBangun(r, hr)
		if err != nil {
			utils.ResponseJSON(w, err.Error(), http.StatusBadRequest)
			return
		}
		tipe, err := utils.CheckTipeHitung(r)
		if err != nil {
			utils.ResponseJSON(w, err.Error(), http.StatusBadRequest)
			return
		}
		chResult := make(chan float64)
		
		switch jenis {
			case utils.SegitigaSamaSisi:
				go utils.HitungSegitigaSamaSisi(tipe, dataBangun, chResult)
			case utils.Persegi:
				go utils.HitungPersegi(tipe, dataBangun, chResult)
			case utils.PersegiPanjang:
				go utils.HitungPersegiPanjang(tipe, dataBangun, chResult)
			case utils.Lingkaran:
				go utils.HitungLingkaran(tipe, dataBangun, chResult)
			case utils.JajarGenjang:
				go utils.HitungJajarGenjang(tipe, dataBangun, chResult)
		}				
		
		calcRes := fmt.Sprintf("%.2f", <-chResult)
		
		res := map[string]string {
			"Hitung" : r.URL.Query().Get("hitung"),
			"Bangun" : jenis,
			"Hasil" : calcRes,
		}
		utils.ResponseJSON(w, res, http.StatusOK)
		return
	}
	http.Error(w, "ERROR...", http.StatusNotFound)
	return
}

// Soal 3
func GetBook(w http.ResponseWriter, r *http.Request, hr httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	
	defer cancel()
	var books []models.Book
	var err error
	if len(r.URL.Query()) > 0 {
		books, err = bookrep.GetAllFiltered(ctx, r)
	} else {
		books, err = bookrep.GetAll(ctx)
	}
	
	if err != nil {
		fmt.Println(err)
	}
	
	utils.ResponseJSON(w, books, http.StatusOK)
}

func PostBook(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var book models.Book
  if err := json.NewDecoder(r.Body).Decode(&book); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  var err_validation []string
  _, err := url.ParseRequestURI(book.ImageURL)
  if err != nil {
	err_validation = append(err_validation, err.Error())
  }
  if book.ReleaseYear < 1980 {
	err_validation = append(err_validation, "Tahun Keluaran harus di antara 1980-2021!")
  } else if book.ReleaseYear > 2021 {
	err_validation = append(err_validation, "Tahun Keluaran harus di antara 1980-2021!")
  }
  
  if len(err_validation) > 0 {
	utils.ResponseJSON(w, err_validation, http.StatusBadRequest)
    return
  }
  total_page, _ := strconv.Atoi(book.TotalPage)
  if total_page >= 201 {
	book.KategoriKetebalan = "Tebal"
  } else if total_page >= 101 && total_page <= 200 {
	book.KategoriKetebalan = "Sedang"
  } else if total_page <= 100 {
	book.KategoriKetebalan = "Tipis"
  }
  
  if err := bookrep.Insert(ctx, book); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Update Book
func UpdateBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }

  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var book models.Book

  if err := json.NewDecoder(r.Body).Decode(&book); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  var err_validation []string
  _, err := url.ParseRequestURI(book.ImageURL)
  if err != nil {
	err_validation = append(err_validation, err.Error())
  }
  if book.ReleaseYear < 1980 {
	err_validation = append(err_validation, "Tahun Keluaran harus di antara 1980-2021!")
  } else if book.ReleaseYear > 2021 {
	err_validation = append(err_validation, "Tahun Keluaran harus di antara 1980-2021!")
  }
  
  if len(err_validation) > 0 {
	utils.ResponseJSON(w, err_validation, http.StatusBadRequest)
    return
  }
  total_page, _ := strconv.Atoi(book.TotalPage)
  if total_page >= 201 {
	book.KategoriKetebalan = "Tebal"
  } else if total_page >= 101 && total_page <= 200 {
	book.KategoriKetebalan = "Sedang"
  } else if total_page <= 100 {
	book.KategoriKetebalan = "Tipis"
  }

  var idBook = ps.ByName("id")
  if err := bookrep.Update(ctx, book, idBook); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }

  res := map[string]string{
    "status": "Succesfully",
  }

  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete Book
func DeleteBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var idBook = ps.ByName("id")
  if err := bookrep.Delete(ctx, idBook); err != nil {
    kesalahan := map[string]string{
      "error": fmt.Sprintf("%v", err),
    }
    utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusOK)
}

// Soal 4
func BasicAuth(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		
		user, password, hasAuth := r.BasicAuth()

		if hasAuth {
			if user == "admin" && password == "password" {
				h(w, r, ps)
			} else if user == "editor" && password == "secret" {
				h(w, r, ps)
			} else if user == "trainer" && password == "rahasia" {
				h(w, r, ps)
			} else {
				utils.ResponseJSON(w, "Username atau password anda salah", http.StatusUnauthorized)
				return
			}
			
		} else {
			utils.ResponseJSON(w, "Anda belum memasukan otentikasi dasar", http.StatusUnauthorized)
			return
		}
	}
}
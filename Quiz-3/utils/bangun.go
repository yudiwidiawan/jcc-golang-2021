package utils

import (
	"net/http"
	"errors"
	"math"
	"strconv"
	"github.com/julienschmidt/httprouter"
)

const (
	SegitigaSamaSisi string = "segitiga-sama-sisi"
	Persegi string = "persegi"
	PersegiPanjang string = "persegi-panjang"
	Lingkaran string = "lingkaran"
	JajarGenjang string = "jajar-genjang"
)

func CheckTipeHitung(r *http.Request) (int, error) {
	switch r.URL.Query().Get("hitung") {
		case "luas":
			return 101, nil
		case "keliling":
			return 102, nil
	}
	return 0, errors.New("Maaf, tipe perhitungan tidak dikenali")
}

func CheckJenisBangun(r *http.Request, hr httprouter.Params) (string, map[string]float64, error) {
	var dataBangun map[string]float64
	dataBangun = map[string]float64{}	
	if hr.ByName("jenis") == SegitigaSamaSisi || hr.ByName("jenis") == Persegi || hr.ByName("jenis") == PersegiPanjang ||
	hr.ByName("jenis") == Lingkaran || hr.ByName("jenis") == JajarGenjang {
		for key, e:= range r.URL.Query() {
			dataBangun[key], _ = strconv.ParseFloat(e[0], 8)
		}
		return hr.ByName("jenis"), dataBangun, nil
	}
	return "", nil, errors.New("Maaf, jenis bangun tidak dikenali")
}

func HitungSegitigaSamaSisi(tipe int, dataBangun map[string]float64, ch chan float64) {
	switch tipe {
		case 101:
			alas := dataBangun["alas"]
			tinggi := dataBangun["tinggi"]
			ch <- ((alas * tinggi)/2)
		case 102:
			alas := dataBangun["alas"]
			ch <- (alas * 3)
		default:
			ch <- 0
	}
	ch <- 0
}

func HitungPersegi(tipe int, dataBangun map[string]float64, ch chan float64) {
	sisi := dataBangun["sisi"]
	switch tipe {
		case 101:
			ch <- (sisi * sisi)
		case 102:
			ch <- (sisi * 4)
		default:
			ch <- 0
	}
	ch <- 0
}

func HitungPersegiPanjang(tipe int, dataBangun map[string]float64, ch chan float64) {
	panjang := dataBangun["panjang"]
	lebar := dataBangun["lebar"]
	switch tipe {
		case 101:
			ch <- (panjang * lebar)
		case 102:
			ch <- (2 * panjang) + (2 * lebar)
		default:
			ch <- 0
	}
	ch <- 0
}

func HitungLingkaran(tipe int, dataBangun map[string]float64, ch chan float64) {
	jariJari := dataBangun["jariJari"]
	diameter := 2*jariJari
	switch tipe {
		case 101:
			ch <- math.Pi * math.Pow(jariJari, 2)
		case 102:
			ch <- 2 * math.Pi * diameter
		default:
			ch <- 0
	}
	ch <- 0
}

func HitungJajarGenjang(tipe int, dataBangun map[string]float64, ch chan float64) {
	alas := dataBangun["alas"]
	sisi := dataBangun["sisi"]
	tinggi := dataBangun["tinggi"]
	switch tipe {
		case 101:
			ch <- alas * tinggi
		case 102:
			ch <- (2 * alas) + (2 * sisi)
		default:
			ch <- 0
	}
	ch <- 0
}
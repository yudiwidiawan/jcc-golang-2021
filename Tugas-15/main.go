package main

import (
	"Tugas-15/utils"
	"Tugas-15/nilaimahasiswa"
	"Tugas-15/models"
	"net/http"
	"log"
	"fmt"
	"context"
	"strconv"
	"encoding/json"
	
	"github.com/julienschmidt/httprouter"
)

func main() {
	router := httprouter.New()
	router.GET("/mahasiswa", GetNilaiMahasiswa)
	router.POST("/mahasiswa/create", PostNilaiMahasiswa)
	router.PUT("/mahasiswa/:id/update", UpdateNilaiMahasiswa)
	router.DELETE("/mahasiswa/:id/delete", DeleteNilaiMahasiswa)
	fmt.Println("Server running on http://localhost:8080/")
	log.Fatal(http.ListenAndServe("127.0.0.1:8080", router))
}

func GetNilaiMahasiswa(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	
	defer cancel()
	dataNilaiMahasiswa, err := nilaimahasiswa.GetAll(ctx)
	
	if err != nil {
		fmt.Println(err)
	}
	
	utils.ResponseJSON(w, dataNilaiMahasiswa, http.StatusOK)
}

func PostNilaiMahasiswa(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var nilaiMhs models.NilaiMahasiswa
  if err := json.NewDecoder(r.Body).Decode(&nilaiMhs); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  converted, errnilai := utils.CheckNilai(int(nilaiMhs.Nilai), nil)
  if errnilai != nil {
	http.Error(w, "Error : " + errnilai.Error(), http.StatusInternalServerError)
	return
  }
  nilaiMhs.IndeksNilai = utils.GetIndeksNilai(converted, w, r)
  if err := nilaimahasiswa.Insert(ctx, nilaiMhs); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Update NilaiMahasiswa
func UpdateNilaiMahasiswa(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }

  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var nilaiMhs models.NilaiMahasiswa

  if err := json.NewDecoder(r.Body).Decode(&nilaiMhs); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  converted, errnilai := utils.CheckNilai(int(nilaiMhs.Nilai), nil)
  if errnilai != nil {
	http.Error(w, "Error : " + errnilai.Error(), http.StatusInternalServerError)
	return
  }
  nilaiMhs.IndeksNilai = utils.GetIndeksNilai(converted, w, r)

  var idMhs = ps.ByName("id")
  convr, _ := strconv.Atoi(idMhs)
  if err := nilaimahasiswa.Update(ctx, nilaiMhs, uint(convr)); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }

  res := map[string]string{
    "status": "Succesfully",
  }

  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete NilaiMahasiswa
func DeleteNilaiMahasiswa(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var idMhs = ps.ByName("id")
  convr, _ := strconv.Atoi(idMhs)
  if err := nilaimahasiswa.Delete(ctx, uint(convr)); err != nil {
    kesalahan := map[string]string{
      "error": fmt.Sprintf("%v", err),
    }
    utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusOK)
}
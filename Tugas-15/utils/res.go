package utils

import (
	"net/http"
	"encoding/json"
	"errors"
)

func ResponseJSON(w http.ResponseWriter, p interface{}, status int) {
	ubahkeByte, err := json.Marshal(p)
	w.Header().Set("Content-Type", "application/json")
	
	if err != nil {
		http.Error(w, "Error", http.StatusBadRequest)
	}
	
	w.WriteHeader(status)
	w.Write([]byte(ubahkeByte))
}

func CheckNilai(nilai int, errorconv error) (int, error) {
	if errorconv != nil {
		return nilai, errorconv
	}
	if nilai > 100 {
		return nilai, errors.New("Maaf, nilai tidak boleh lebih dari 100.")
	} else if nilai <= -1 {
		return nilai, errors.New("Maaf, nilai tidak boleh kurang dari 0.")
	}
	return nilai, nil
}

func GetIndeksNilai(nilai int, w http.ResponseWriter, r *http.Request) string {
	switch {
		case nilai >= 80:
			return "A"
		case nilai >= 70 && nilai < 80:
			return "B"
		case nilai >= 60 && nilai < 70:
			return "C"
		case nilai >= 50 && nilai < 60:
			return "D"
		case nilai < 50:
			return "E"
	}
	return "F"
}
package nilaimahasiswa

import (
	"Tugas-15/config"
	"Tugas-15/models"
	"context"
	"fmt"
	"log"
	"time"
	"database/sql"
	"errors"
)

const (
	table = "nilai_mahasiswa"
	layoutDateTime = "2006-01-02 15:04:05"
)
// Fetch All Data Mahasiswa
func GetAll(ctx context.Context) ([]models.NilaiMahasiswa, error) {
	var dataNilaiMahasiswa []models.NilaiMahasiswa
	db, err := config.MySQL()
	
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}
	
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at ASC", table)
	rowQuery, err := db.QueryContext(ctx, queryText)
	
	if err != nil {
		log.Fatal(err)
	}
	
	for rowQuery.Next() {
		var nilaiMahasiswa models.NilaiMahasiswa
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&nilaiMahasiswa.ID,
		&nilaiMahasiswa.Nama,
		&nilaiMahasiswa.MataKuliah,
		&nilaiMahasiswa.Nilai,
		&nilaiMahasiswa.IndeksNilai,
		&createdAt,
		&updatedAt); err != nil {
			return nil, err
		}
		nilaiMahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		
		if err != nil {
			log.Fatal(err)
		}
		
		nilaiMahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		
		if err != nil {
			log.Fatal(err)
		}
		
		dataNilaiMahasiswa = append(dataNilaiMahasiswa, nilaiMahasiswa)
	}
	return dataNilaiMahasiswa, nil
}

// Insert Data Nilai Mahasiswa
func Insert(ctx context.Context, nilaiMahasiswa models.NilaiMahasiswa) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }
  
  queryText := fmt.Sprintf("INSERT INTO %v (nama, matakuliah, nilai, indeks_nilai, created_at, updated_at) values('%v','%v',%v,'%v', NOW(), NOW())", table,
    nilaiMahasiswa.Nama,
    nilaiMahasiswa.MataKuliah,
    nilaiMahasiswa.Nilai,
    nilaiMahasiswa.IndeksNilai)
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update Nilai Mahasiswa
func Update(ctx context.Context, nilaiMahasiswa models.NilaiMahasiswa, id uint) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set nama ='%s', matakuliah = '%s', nilai = %d, indeks_nilai = '%s', updated_at = NOW() where id = %d",
    table,
    nilaiMahasiswa.Nama,
    nilaiMahasiswa.MataKuliah,
    nilaiMahasiswa.Nilai,
    nilaiMahasiswa.IndeksNilai,
    id,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Nilai Mahasiswa
func Delete(ctx context.Context, id uint) error {
    db, err := config.MySQL()
    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %d", table, id)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}
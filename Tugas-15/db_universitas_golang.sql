-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Sep 2021 pada 04.06
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_universitas_golang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_mahasiswa`
--

CREATE TABLE `nilai_mahasiswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(80) NOT NULL,
  `matakuliah` varchar(255) NOT NULL,
  `nilai` int(10) UNSIGNED NOT NULL,
  `indeks_nilai` varchar(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `nilai_mahasiswa`
--

INSERT INTO `nilai_mahasiswa` (`id`, `nama`, `matakuliah`, `nilai`, `indeks_nilai`, `created_at`, `updated_at`) VALUES
(1, 'Yudi Widiawan', 'Matematika', 50, 'D', '2021-09-18 08:58:21', '2021-09-18 09:02:59'),
(2, 'Ahmad', 'Algoritma', 70, 'B', '2021-09-18 08:59:17', '2021-09-18 08:59:17'),
(3, 'Deni', 'Algoritma', 60, 'C', '2021-09-18 08:59:35', '2021-09-18 08:59:35'),
(4, 'Arsy', 'Algoritma', 50, 'D', '2021-09-18 08:59:49', '2021-09-18 08:59:49'),
(5, 'Dean', 'Algoritma', 40, 'E', '2021-09-18 09:00:11', '2021-09-18 09:00:11');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `nilai_mahasiswa`
--
ALTER TABLE `nilai_mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `nilai_mahasiswa`
--
ALTER TABLE `nilai_mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package main

import "fmt"

// soal 1
type buah struct {
	nama, warna string
	adaBijinya bool
	harga int
}
// soal 2
type segitiga struct{
  alas, tinggi int
}

type persegi struct{
  sisi int
}

type persegiPanjang struct{
  panjang, lebar int
}
func (s segitiga) luasSegitiga() int{
	return (s.alas*s.tinggi)/2
}
func (p persegi) luasPersegi() int{
	return p.sisi*p.sisi
}
func (pp persegiPanjang) luasPersegiPanjang() int {
	return pp.panjang*pp.lebar
}
// soal 3
type phone struct{
   name, brand string
   year int
   colors []string
}
func (ph *phone) addColors(warnas ...string) []string {
	for _, warna := range warnas {
		ph.colors = append(ph.colors, warna)
	}
	return ph.colors
}
// soal 4
type movie struct {
	title, genre string
	duration, year int
} 

func main () {
	// soal 1
	var nanas = buah{"Nanas", "Kuning", false, 9000}
	var jeruk = buah{"Jeruk", "Oranye", true, 8000}
	var semangka = buah{"Semangka", "Hijau & Merah", true, 10000}
	var pisang = buah{"Pisang", "Kuning", false, 5000}
	var buahs = []buah{nanas, jeruk, semangka, pisang};
	fmt.Println("Nama\t\tWarna\t\tAda Bijinya\tHarga")
	for _, e := range buahs {
		var temp string
		if e.adaBijinya {
			temp = "Ada"
		} else {
			temp = "Tidak"
		}
		if len(e.warna) > 10 {
			fmt.Printf("%s\t%s\t%s\t\t%d\n", e.nama, e.warna, temp, e.harga)
		} else {
			fmt.Printf("%s\t\t%s\t\t%s\t\t%d\n", e.nama, e.warna, temp, e.harga)
		}
	}
	fmt.Println()
	
	// soal 2
	var segitigaSatu = segitiga{4,7}
	fmt.Printf("Alas : %d, Tinggi : %d\n", segitigaSatu.alas, segitigaSatu.tinggi)
	fmt.Println("Luas Segitiga = ", segitigaSatu.luasSegitiga())
	var persegiSatu = persegi{5}
	fmt.Printf("Sisi : %d\n", persegiSatu.sisi)
	fmt.Println("Luas Persegi = ", persegiSatu.luasPersegi())
	var persegiPanjangSatu = persegiPanjang{5,10}
	fmt.Printf("Panjang : %d, Lebar : %d\n", persegiPanjangSatu.panjang, persegiPanjangSatu.lebar)
	fmt.Println("Luas Persegi Panjang = ", persegiPanjangSatu.luasPersegiPanjang())
	fmt.Println()
	
	// soal 3
	var iphone = phone{name: "iPhone X", brand: "iphone", year: 2019}
	iphone.addColors("Merah", "Hitam", "Blue", "Silver", "Gold")
	fmt.Printf("Nama : %s\nMerk : %s\nTahun : %d\nWarna Tersedia:%v\n\n", iphone.name,
	iphone.brand,iphone.year,iphone.colors)
	
	// soal 4
	var dataFilm = []movie{}
	var tambahDataFilm = func (tl string, drn int, gnr string, yr int, data *[]movie) {
		temp := movie{title: tl, genre: gnr, duration: drn, year: yr}
		*data = append(*data, temp)
	}
	tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
	tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
	tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
	tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)
	
	for i, item := range dataFilm {
		fmt.Printf("%d. title : %s\n   duration : %d\n   genre : %s\n   year : %d\n", (i+1), item.title,
		item.duration, item.genre, item.year)
		fmt.Println()
	}
	
}